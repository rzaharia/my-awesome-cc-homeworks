import json
import os
import time

import requests

request_id = 1


class host:
    def __init__(self, host, request=None, headers=None):
        self.host = host
        self.request = request
        self.headers = headers
        self.text = ''

    def __str__(self):
        result = self.host
        if self.request is not None:
            result += "\n" + json.dumps(self.request)
        if self.headers is not None:
            result += "\n" + json.dumps(self.headers)
        return result


class host_cats(host):
    def __init__(self, key):
        super().__init__('https://api.thecatapi.com/v1/breeds', {'attach_breed': 0},
                         {'x-api-key': key})


class host_dogs(host):
    def __init__(self):
        super().__init__('https://dog.ceo/api/breeds/list/all')


class host_sentiment(host):
    def __init__(self, text1, text2):
        super().__init__('https://api.datamuse.com/words', {'ml': text1 + " " + text2})


def get_method(host, id, starting_char):
    request = "GET " + str(host) + "\n"
    start_time = time.time()
    result = requests.get(host.host, host.request, timeout=10, headers=host.headers)
    final_time = time.time() - start_time
    host.text = result.text
    response = "Response: " + "\n" + result.text + "\n"
    file_name = str(id).rjust(5, '0') + ".txt"
    with open(os.path.join(starting_char, file_name), 'wb') as f:
        f.write(("Time: " + str(final_time) + "\n").encode())
        f.write(request.encode())
        f.write(response.encode())
    return result


def get_list(items, header):
    result = '<h1>' + header + '</h1>'
    result += '<ul>'
    for item in items:
        '<a href="/cats">Click here to view Folder</a>'
        href_item = '<a href="/cats/' + item + '">' + item + '</a>'
        result += '<li>' + href_item + '</li>'
    result += '</ul>'
    return result


def get_folder(folder_name):
    return get_list(os.listdir(folder_name), 'Current log folder ' + folder_name).encode()
