import os, json

from functions import get_method
from functions import host_cats
from functions import host_dogs
from functions import host_sentiment

from functions import get_folder

import threading, json, concurrent.futures, urllib.request
from http.server import BaseHTTPRequestHandler, HTTPServer
import yaml

PORT_NUMBER = 80
public_folders = ['/cats', '/dogs', '/sen']
functions = ['/do_all', '/metrics']
cats_key = yaml.load(open('config.yaml'))['cats_key']


def metrics():
    folder_names = [folder[1:] for folder in public_folders]
    result = '{'
    for folder in folder_names:
        current_list = []
        for file in os.listdir(folder):
            with open(os.path.join(folder, file), 'rb') as f:
                line = f.readline().decode()[6:].strip()
                current_list.append(line)
        min_value = min(current_list)
        max_value = max(current_list)
        result += '\"' + folder + '\": {'
        result += '\"min_time\": ' + str(min_value) + ','
        result += '\"max time\": ' + str(max_value) + ''
        result += '},'
    result = result[:-1]
    result += '}'
    return result


def get_logs(id):
    cats = host_cats(cats_key)
    dogs = host_dogs()
    get_method(cats, id, 'cats')
    get_method(dogs, id, 'dogs')
    sen = host_sentiment(json.loads(cats.text)[0]['temperament'], json.loads(dogs.text)['status'])
    get_method(sen, id, 'sen')


def do_all_logs(starting_id):
    with concurrent.futures.ThreadPoolExecutor(max_workers=50) as a:
        result = a.map(get_logs, [i for i in range(starting_id, starting_id + 50)])
        print(len([r for r in result]))


def do_single(self):
    get_logs(0)
    f = open(os.curdir + os.sep + 'cats/00000.txt', 'rb')
    self.send_response(200)
    self.send_header('Content-type', 'text')
    self.end_headers()
    self.wfile.write(f.read())
    f.close()
    f = open(os.curdir + os.sep + 'dogs/00000.txt', 'rb')
    self.wfile.write(f.read())
    f.close()
    f = open(os.curdir + os.sep + 'sen/00000.txt', 'rb')
    self.wfile.write(f.read())
    f.close()


class myHandler(BaseHTTPRequestHandler):

    # Handler for the GET requests
    def do_GET(self):
        is_folder = False
        if self.path == "/":
            self.path = "/index.html"
        elif self.path in public_folders:
            is_folder = True
        elif self.path.startswith('/do_all_'):
            do_all_logs(int(self.path.split('_')[2]))
            self.path = "/index.html"
        elif self.path == '/metrics':
            self.send_response(200)
            self.send_header('Content-type', 'application/json')
            self.end_headers()
            self.wfile.write(metrics().encode())
            return
        elif self.path == '/do_single':
            do_single(self)
            self.send_response(200)
            self.send_header('Content-type', 'text/html')
            self.end_headers()
            return
        try:
            # Check the file extension required and
            # set the right mime type

            sendReply = False
            if self.path.endswith(".html"):
                mimetype = 'text/html'
                sendReply = True
            elif self.path.endswith(".txt"):
                mimetype = 'text'
                sendReply = True

            if sendReply:
                # Open the static file requested and send it
                f = open(os.curdir + os.sep + self.path, 'rb')
                self.send_response(200)
                self.send_header('Content-type', mimetype)
                self.end_headers()
                self.wfile.write(f.read())
                f.close()
            elif is_folder:
                self.send_response(200)
                self.send_header('Content-type', 'text/html')
                self.end_headers()
                self.wfile.write(get_folder(self.path[1:]))
            return

        except IOError:
            self.send_error(404, 'File Not Found: %s' % self.path)


server = None
try:
    # Create a web server and define the handler to manage the
    # incoming request
    server = HTTPServer(('', PORT_NUMBER), myHandler)
    print('Started httpserver on port ', PORT_NUMBER)

    # Wait forever for incoming htto requests
    server.serve_forever()
except KeyboardInterrupt:
    server.socket.close()

# import requests

# host = 'http://www.splashbase.co'
# route = '/api/v1/images/search'
# dict_request = {"query": "laptop"}
# result = requests.get(host + route, dict_request, timeout=10, headers={'content-type': 'application/json'})


# print(result)
# print(result.text)


# import threading, json, concurrent.futures, urllib.request
#
# with concurrent.futures.ThreadPoolExecutor(max_workers=50) as a:
#     result = a.map(urllib.request.urlopen, ["http://www.splashbase.co" for _ in range(50)])
#     print([r for r in result])
