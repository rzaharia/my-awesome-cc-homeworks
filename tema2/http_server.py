import json
from http.server import BaseHTTPRequestHandler, HTTPServer

import db

database, c = db.open_db()

PORT_NUMBER = 80


def get_json(value):
    return json.loads(value)


def get_result_json(value):
    return ("{\"result\":\"%s\"}" % value).encode()


def adjust_request(path, body=None):
    result = {'link': path}
    if body:
        try:
            result['body'] = json.loads(body)
        except:
            return False
    splitted_path = path.split('/')
    result['table'] = splitted_path[1]
    if len(splitted_path) > 2:
        result['field'] = splitted_path[2]
    else:
        result['field'] = ''
    return result


def send_api_json_generic_result(obj, ok, r_value, result):
    obj.send_response(r_value)
    obj.send_header('Content-type', 'application/json')
    obj.end_headers()
    if not ok:
        obj.wfile.write(get_result_json(result))
    else:
        obj.wfile.write(result.encode())


class myHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        ok, r_value, result = db.get_data(adjust_request(self.path), c)
        send_api_json_generic_result(self, ok, r_value, result)

    def do_POST(self):
        content_length = int(self.headers['Content-Length'])
        post_data = self.rfile.read(content_length).decode()
        result = adjust_request(self.path, post_data)
        ok, r_value, result = db.post_data(result, c, database)
        send_api_json_generic_result(self, ok, r_value, result)

    def do_DELETE(self):
        ok, r_value, result = db.delete_data(adjust_request(self.path), c, database)
        send_api_json_generic_result(self, ok, r_value, result)

    def do_PUT(self):
        content_length = int(self.headers['Content-Length'])
        post_data = self.rfile.read(content_length).decode()
        ok, r_value, result = db.put_data(adjust_request(self.path, post_data), c, database)
        send_api_json_generic_result(self, ok, r_value, result)


server = None

try:
    # Create a web server and define the handler to manage the
    # incoming request
    server = HTTPServer(('', PORT_NUMBER), myHandler)
    print('Started httpserver on port ', PORT_NUMBER)

    # Wait forever for incoming htto requests
    server.serve_forever()
except KeyboardInterrupt:
    server.socket.close()
    db.close_db(database, c)
