import json
import sqlite3

tables = {
    'files': ['id', 'name', 'location', 'size'],
    'rights': ['id', 'user_id', 'file_id', 'rights'],
    'users': ['id', 'username', 'mail']
}


def get_ok_result_json(value):
    return "{\"ok result\":\"%s\"}" % value


def check_ALL_request_ok(request):
    if request['table'] in tables.keys():
        body_columns = [field for field in request['body'].keys()]
        if 'id' not in body_columns and request['field'] is not '':
            body_columns.insert(0, 'id')
            request['body']['id'] = request['field']
        db_columns = tables[request['table']]
        if body_columns == db_columns:
            return True
    return False


def check_medium(request):
    body_columns = [field.lower() for field in request['body'].keys()]
    if len(body_columns) == 0 or (len(body_columns) == 1 and 'id' in body_columns):
        return False
    if 'id' not in body_columns and request['field'] is '':
        return False
    db_columns = tables[request['table']]
    for field in body_columns:
        if field not in db_columns:
            return False
    return True


def check_minimum(request):
    if request['table'] in tables.keys():
        return True
    return False


def put_data(request, c, database):
    if not check_minimum(request):
        return False, 404, "Table " + request['table'] + " not found"
    if request['field'] is '':
        return False, 405, "Method Not Allowed!"
    if not check_medium(request):
        return False, 400, "Incorrect fields!"

    query = 'SELECT * FROM ' + request['table'] + ' WHERE ID = ?'
    try:
        c.execute(query, (request['field'],))
        result = c.fetchall()
        if len(result) == 0:
            return False, 404, "Resource not found"
    except:
        return False, 404, "Resource not found"

    values = [key + "='" + value + "'" for key, value in request['body'].items() if key != 'id']
    query = 'UPDATE ' + request['table'] + ' SET ' + ','.join(values) + "WHERE id=?"
    c.execute(query, (request['field'],))
    database.commit()
    return True, 200, get_ok_result_json("Resource updated succesfully!!!")


def delete_data(request, c, database):
    if not check_minimum(request):
        return False, 404, "Table " + request['table'] + " not found"
    if request['field'] is '':
        return False, 405, "Method Not Allowed!"

    query = 'SELECT * FROM ' + request['table'] + ' WHERE ID = ?'
    try:
        c.execute(query, (request['field'],))
        result = c.fetchall()
        if len(result) == 0:
            return False, 404, "Resource not found"
    except:
        return False, 404, "Resource not found"

    query = 'DELETE FROM ' + request['table'] + ' WHERE ID = ?'
    try:
        c.execute(query, (request['field'],))
        database.commit()
    except:
        return False, 404, "Resource not found"
    return True, 200, get_ok_result_json("Resource deleted!")


def post_data(request, c, database):
    if request is False:
        return False, 415, "Unsupported Media Type"
    if not check_minimum(request):
        return False, 404, "Table " + request['table'] + " not found"
    if not check_ALL_request_ok(request):
        return False, 400, "Incorrect fields!"

    query = 'INSERT INTO ' + request['table'] + ' ('
    query += ','.join(tables[request['table']])
    query += ') VALUES ('
    last_id = len(tables[request['table']])
    fields = []
    for id, field in enumerate(tables[request['table']]):
        fields.append(request['body'][field])
        query += '?'
        if id != last_id - 1:
            query += ', '
    query += ')'
    try:
        c.execute(query, fields)
        database.commit()
    except sqlite3.IntegrityError:
        return False, 409, "Resource already exists!"
    return True, 201, get_ok_result_json("Resource created!")


def get_data(request, c):
    if not check_minimum(request):
        return False, 404, "Table " + request['table'] + " not found"
    result = request['link'].split('/')
    query = 'SELECT * FROM ' + request['table']
    if len(result) > 2:
        if result[2] is not '':
            query += ' WHERE id' + ' = ?'
    try:
        c.execute(query, request['field'])
        result = c.fetchall()
        result_final = []
        table_keys = tables[request['table']]
        for entry in result:
            temp_r = {}
            for value, name in zip(entry, table_keys):
                temp_r[name] = value
            result_final.append(temp_r)
        return True, 200, json.dumps(result_final)
    except:
        return False, 404, "Field not found"


def open_db():
    db = sqlite3.connect('lab9.db')
    return db, db.cursor()


def close_db(db, c):
    c.close()
    db.close()
