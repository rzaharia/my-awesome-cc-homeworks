from flask import Flask, render_template, request, jsonify, make_response, json
from pusher import pusher
from google.cloud import storage
import logging
import time
from google.cloud import monitoring_v3

app = Flask(__name__)

pusher = pusher_client = pusher.Pusher(
    app_id='',
    key='',
    secret='',
    cluster='eu',
    ssl=True
)


@app.errorhandler(500)
def server_error(e):
    logging.exception('An error occurred during a request.')
    return """
        An internal error occurred: <pre>{}</pre>
        See logs for full stacktrace.
        """.format(e), 500


@app.route('/monitor')
def monitor():
    client = monitoring_v3.MetricServiceClient()
    project = 'funcloudcomputing'
    project_name = client.project_path(project)

    series = monitoring_v3.types.TimeSeries()
    series.metric.type = 'custom.googleapis.com/my_metric'
    series.resource.type = 'gce_instance'
    series.resource.labels['instance_id'] = ''
    series.resource.labels['zone'] = 'us-central1-f'
    point = series.points.add()
    point.value.double_value = 3.14
    now = time.time()
    point.interval.end_time.seconds = int(now)
    point.interval.end_time.nanos = int(
        (now - point.interval.end_time.seconds) * 10 ** 9)
    client.create_time_series(project_name, [series])

    result_list = ['Succesfully wrote time series']
    result_list.extend([element for element in client.list_monitored_resource_descriptors(project_name)])
    return str(result_list)


@app.route('/logs')
def show_logs():
    storage_client = storage.Client()

    bucket_name = 'logs_bucket_909'

    bucket = storage_client.get_bucket(bucket_name)

    blob = bucket.blob(r"")

    blob.download_to_filename(r"./loguri.txt")

    file = open(r"./loguri.txt")
    contents = file.read()
    return contents


name = ''


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/play')
def play():
    global name
    name = request.args.get('username')
    return render_template('play.html')


@app.route("/pusher/auth", methods=['POST'])
def pusher_authentication():
    auth = pusher.authenticate(
        channel=request.form['channel_name'],
        socket_id=request.form['socket_id'],
        custom_data={
            u'user_id': name,
            u'user_info': {
                u'role': u'player'
            }
        }
    )
    return json.dumps(auth)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)

name = ''
